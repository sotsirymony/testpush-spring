package com.example.demo.repository;

import com.example.demo.repository.DTO.Article;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {
    @Update("UPDATE tbl_article SET code=#{code},title=#{title},description=#{description} WHERE code=#{code}")
    boolean update(Article article);
    @Insert("INSERT INTO tbl_article (code,title,description) VALUES (#{code},#{title},#{description})")
    boolean insert(Article article);
    @Select("SELECT * FROM tbl_article")
    List<Article> getData();
    @Delete("DELETE FROM tbl_article WHERE code=#{code}")
    boolean delete(String code);
    @Select("SELECT code FROM tbl_article WHERE code=#{code}")
    Article findCode(String code);

}
